<x-frontend.layouts.master>
<div class="bg-light p-5 rounded">
<div class="row row-cols-1 row-cols-md-3 mb-3 text-center">

@foreach ($products as $product)
      <div class="col">
        <div class="card mb-4 rounded-3 shadow-sm">
          <div class="card-header py-3">
           <img src="{{asset('storage/products/' .$product->image) }}" />
          </div>
          <div class="card-body">
            <h4 class="card-title pricing-card-title">
            <a href="" > {{  Str::limit($product->title, 20)  }}</a>
            </h4>
            <p>{{ $product->price }} TK</p>
            <button type="button" class="w-100 btn btn-lg btn-outline-primary">Add to cart</button>
          </div>
        </div>
      </div>
   @endforeach

    </div>
     {{ $products->links() }}
</div>
</x-frontend.layouts.master>