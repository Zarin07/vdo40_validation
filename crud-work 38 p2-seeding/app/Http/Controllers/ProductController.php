<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Database\QueryException;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index()
    {
    //   $products = Product::all();
    $products = Product::orderBy('id', 'desc')->get();
    //    dd($products);
        return view('backend.products.index', compact('products'));
    }


    public function create(){

        return view('backend.products.create');
    }

    public function store(ProductRequest $request)
    
    {
        try{

            //  $request->validate([
            //     'title' => 'required |min:10|unique:products,title',
            //     'price' => 'required|numeric'                           /// one way to do validation
            // ]);

             
          
            // Product::create([
            //     'title' => $request->title,
            //     'price' => $request->price
            // ]);                                 //this is one way
            

            Product::create($request->all());   //this is create method, shortcut way. in most of the case i will use it

            // $product = new Product();
            // $product->title = $request->title;
            // $product->price = $request->price;
            // $product->save();                     // this is save method to add data  in db


            //  DB::table('products')->insert([
            //      'title' => $request->title,
            //      'price' => $request->price
            //  ]);                                   // this is insert method to save data in the db



            return redirect()->route('products.index');

        } catch(QueryException $e){
             return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

       
    }

    public function show($id){
        // dd($id);
        $productName = 'T-shirt';
        return view('products.show', compact('id', 'productName'));
    }
}
