<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;  // false thakbe. eitake true bole dite hobe
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required |min:10|max:255|unique:products,title',
           'price' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return[
            'title.required' => 'ekhane title obossoi dite hobe'
        ];
    }
}
